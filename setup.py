from setuptools import setup, find_packages

setup(name='pokeraid_betstring',
      version='0.1',
      description='Pokeraid betstring lib',
      url='https://bitbucket.org/m-stachura/pokeraid_betstring',
      author='PokerAid',
      author_email='admin@pokeraid.net',
      license='MIT',
      packages=find_packages())
