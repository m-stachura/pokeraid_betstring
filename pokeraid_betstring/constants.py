class Action(object):
    ANTE = 'ante'
    SMALL_BLIND = 'small blind'
    SECOND_SMALL_BLIND = 'secondsb'
    BIG_BLIND = 'big blind'
    BOTH = 'both'
    CALLS = 'calls'
    RAISES = 'raises'
    BETS = 'bets'
    STANDS_PAT = 'stands pat'
    FOLDS = 'folds'
    CHECKS = 'checks'
    DISCARDS = 'discards'
    BRING_IN = 'bringin'
    COMPLETES = 'completes'
    SHOWS = 'shows'
    DOESNT_SHOW = 'doesnt shows'
    HOLE_CARDS = 'hole cards'
    FLOP = 'flop'
    TURN = 'turn'
    RIVER = 'river'
    SHOW_DOWN = 'show down'
    SUMMARY = 'summary'
    DEALT = 'dealt'
    COLLECTED = 'collected'
    TOTAL_POT = 'total pot'
    UNCALLED_BET = 'uncalled bet'
    TIMED_OUT = 'timed out'
    SITTING_OUT = 'sitting out'
    RETURNED = 'returned'
    FINISHED_TOURNAMENT = 'finished tournament'
    REBUY = 're-buy'


class Stage(object):
    PREFLOP = 0
    FLOP = 1
    TURN = 2
    RIVER = 3
    SHOWDOWN = 4
