from collections import deque
from pokeraid_betstring.constants import Action, Stage


def create_hand_betstring(hand_info, hand_actions_info, hand_players_info):
    h_betstring = {
        'h_id': hand_info['h_siteid'],
        'preflop': None,
        'flop': None,
        'turn': None,
        'river': None
    }
    stages = []
    bstr = ''
    p_stacks = {hp['player']: hp['hp_chips'] for hp in hand_players_info}
    p_cur_stacks = p_stacks.copy()
    stack2nd_size = sorted(p_stacks.values(), reverse=True)[1]
    p_actions_cnt = {hp['player']: 0 for hp in hand_players_info}
    p_actions_first_act_indx = dict.fromkeys(p_actions_cnt.keys(), 0)  # tracking first action index in betstring
    p_bstrings = {k: {'act': ['']*4, 'pre_act1': ['']*4, 'pre_act2': ['']*4} for k in p_actions_cnt.keys()}
    betting_round = 1
    nf_preflop = False  # non fold action occured preflop
    prev_actions = deque([], 2)  # keeping last two actions on street buffer

    for action in hand_actions_info:
        #  Reset street vars, change stage, update stacks
        if action['action'] in [Action.FLOP, Action.TURN, Action.RIVER, Action.SHOW_DOWN, Action.SUMMARY]:
            stages.append(bstr)
            bstr = ''
            prev_actions.clear()
            betting_round = 1
            p_actions_cnt = dict.fromkeys(p_actions_cnt.keys(), 0)
            p_actions_first_act_indx = dict.fromkeys(p_actions_cnt.keys(), 0)
            p_stacks.update(p_cur_stacks)
            stack2nd_size = sorted(p_stacks.values(), reverse=True)[1]
        if action['action'] == Action.SUMMARY:  # end loop
            break
        #  Count voluntary actions
        if action['player'] is not None and action['action'] in [Action.CALLS, Action.FOLDS, Action.BETS, Action.RAISES,
                                                                 Action.CHECKS]:
            p_actions_cnt[action['player']] += 1
            betting_round = max(betting_round, p_actions_cnt[action['player']])
            if betting_round == 1:
                p_actions_first_act_indx[action['player']] = len(bstr)
        #  Resolve betstring action
        if action['action'] in [Action.CALLS, Action.FOLDS, Action.BETS, Action.RAISES, Action.CHECKS]:
            if action['ha_stage'] == Stage.PREFLOP:
                if betting_round == 1:
                    if not nf_preflop:
                        act = resolve_init_action(action['action'], action['ha_ammount'], action['ha_ammountTo'],
                                                  hand_info['h_bbSize'], stack2nd_size, action['ha_playerstack'])
                        prev_actions.append(act)
                        bstr += act
                        nf_preflop = False if act == 'F' else True
                    else:
                        act = resolve_next_action(action['action'], action['ha_ammount'], action['ha_ammountTo'],
                                                  action['ha_pot_bet_size'], hand_info['h_bbSize'], stack2nd_size,
                                                  prev_actions, action['ha_playerstack'])
                        prev_actions.append(act)
                        bstr += act
                else:
                    act = resolve_simple_action(action['action'], action['ha_ammount'], stack2nd_size, prev_actions)
                    prev_actions.append(act)
                    bstr += act
            else:
                # later streets betstrings
                act = resolve_normal_action(action['action'], action['ha_ammount'], action['ha_ammountTo'],
                                            action['ha_pot_bet_size'], hand_info['h_bbSize'], stack2nd_size,
                                            prev_actions, action['ha_playerstack'])
                prev_actions.append(act)
                bstr += act

            #  Update players actions and preactions
            if p_bstrings[action['player']]['act'][action['ha_stage']] == '':
                p_bstrings[action['player']]['pre_act1'][action['ha_stage']] = bstr[:-1]

                # change specific spots
                pre_act1 = p_bstrings[action['player']]['pre_act1'][action['ha_stage']]
                if action['ha_stage'] == Stage.PREFLOP and len(pre_act1) == 2 and pre_act1[0] in ['6', '9'] and \
                                pre_act1[1] in ['1', '2', '3', '4', '6', '9']:
                    p_bstrings[action['player']]['pre_act1'][action['ha_stage']] = pre_act1[0] + '6'

            elif len(p_bstrings[action['player']]['act'][action['ha_stage']]) == 1:
                first_act_indx = p_actions_first_act_indx[action['player']]
                p_bstrings[action['player']]['pre_act2'][action['ha_stage']] = bstr[first_act_indx+1:-1]

                # change specific spots
                pre_act2 = p_bstrings[action['player']]['pre_act2'][action['ha_stage']]
                if action['ha_stage'] == Stage.PREFLOP and len(pre_act2) == 2 and pre_act2[0] in ['6', '9'] and \
                                pre_act2[1] in ['1', '2', '3', '4', '6', '9'] and \
                                p_bstrings[action['player']]['act'][action['ha_stage']] == 'C':
                    p_bstrings[action['player']]['pre_act2'][action['ha_stage']] = pre_act2[0] + 'L'

            p_bstrings[action['player']]['act'][action['ha_stage']] += prev_actions[-1] if len(prev_actions) > 0 else ''

        # update stack2_size
        if action['action'] == Action.FOLDS:  # when someone fold then we immediately update 2nd stack size or stop
            del p_stacks[action['player']]
            if len(p_stacks) > 1:
                stack2nd_size = sorted(p_stacks.values(), reverse=True)[1]
        if action['action'] in [Action.ANTE, Action.SMALL_BLIND, Action.BIG_BLIND, Action.CALLS, Action.BETS,
                                Action.RAISES]:  # when there are chips spent then update 2nd stack after street end
            p_cur_stacks[action['player']] -= action['ha_ammount']

    h_betstring.update(dict(zip(['preflop', 'flop', 'turn', 'river'], stages)))
    return h_betstring, p_bstrings


def resolve_init_action(action, bet_size, total_bet_size, bb_size, stack2nd_size, playerstack):
    if action == Action.FOLDS:
        return 'F'
    if action in [Action.CALLS, Action.CHECKS]:
        return 'C'
    if 0.6*playerstack >= total_bet_size > 4.1*bb_size and total_bet_size < 0.65*stack2nd_size:
        return '6'
    if 0.5*playerstack < bet_size and total_bet_size < 0.65*stack2nd_size:
        return '9'
    if total_bet_size < 2.2*bb_size:
        return '1'
    if total_bet_size < 3.1*bb_size:
        return '2'
    if total_bet_size < 4.1*bb_size:
        return '3'
    return 'A'


def resolve_next_action(action, bet_size, total_bet_size, pot_bet_size, bb_size, stack2nd_size, prev_actions,
                        playerstack):
    if action == Action.FOLDS:
        return 'F'
    if action in [Action.CHECKS, Action.CALLS]:
        return 'C'
    # call, or raise after allin is treated as Call
    if 'A' in prev_actions:
        return 'C'
    if total_bet_size > 0.65*stack2nd_size:
        return 'A'
    if 1.2*pot_bet_size >= total_bet_size > 0.8*playerstack:
        return '9'
    if bet_size < 1.2*bb_size:
        return '1'
    if total_bet_size <= 0.4*pot_bet_size:
        return '2'
    if total_bet_size <= 0.8*pot_bet_size:
        return '3'
    if total_bet_size <= 1.2*pot_bet_size:
        return '4'
    return '6'


def resolve_simple_action(action, bet_size, stack2nd_size, prev_actions):
    if action == Action.FOLDS:
        return 'F'
    if action in [Action.CALLS, Action.CHECKS]:
        return 'C'
    if 'A' in prev_actions:
        return 'C'
    if bet_size <= 0.3*stack2nd_size:
        return 'B'
    return 'L'


def resolve_normal_action(action, bet_size, total_bet_size, pot_bet_size, bb_size, stack2nd_size, prev_actions,
                          playerstack):
    if action == Action.FOLDS:
        return 'F'
    if action in [Action.CHECKS, Action.CALLS]:
        return 'C'
    # call, or raise after allin is treated as Call
    if 'A' in prev_actions:
        return 'C'
    size = total_bet_size or bet_size
    if size > 0.65*stack2nd_size:
        return 'A'
    if 1.2*pot_bet_size >= size > 0.8*playerstack:
        return '9'
    if bet_size < 1.2*bb_size:
        return '1'
    if size <= 0.4*pot_bet_size:
        return '2'
    if size <= 0.8*pot_bet_size:
        return '3'
    if size <= 1.2*pot_bet_size:
        return '4'
    return '6'
